<?php
declare(strict_types=1);


namespace App\Aoc14;

final class Polymer
{
    private string $template;
    private array $formel;
    private int $step;
    private array $pairs = [];
    private array $chars = [];

    private function __construct(string $template, array $formel)
    {
        $this->template = $template;
        $this->formel = $formel;
        $this->step = 0;
        $this->init();
    }

    private function init(): void
    {
        $chars = str_split($this->template);
        $counter = strlen($this->template);

        for ($c = 0; $c < $counter - 1; $c++) {
            $pair = sprintf('%s%s', $chars[$c], $chars[$c + 1]);
            $this->addPair($pair, 1);
        }

        foreach ($chars as $char) {
            $this->addChar($char, 1);
        }
    }

    private function addPair(string $pair, int $count): void
    {
        array_key_exists($pair, $this->pairs) || $this->pairs[$pair] = 0;
        $this->pairs[$pair] += $count;
    }

    private function addChar(string $char, int $count): void
    {
        array_key_exists($char, $this->chars) || $this->chars[$char] = 0;
        $this->chars[$char] += $count;
    }

    public static function create(string $template, array $formel): self
    {
        return new self($template, $formel);
    }

    public function getQuantity(): int
    {
        return max($this->chars) - min($this->chars);
    }

    public function step(): void
    {
        foreach ($this->pairs as $pair => $count) {
            $this->splitPair($pair, $count);
        }

        $this->gc();
        ++$this->step;
    }

    private function splitPair(string $pair, int $count): void
    {
        if (!$this->caSplit($pair)) {
            return;
        }
        $this->reducePair($pair, $count);

        $chars = str_split($pair);
        $newPair1 = sprintf('%s%s', $chars[0], $this->formel[$pair]);
        $newPair2 = sprintf('%s%s', $this->formel[$pair], $chars[1]);
        foreach ([$newPair1, $newPair2] as $newPair) {
            $this->addPair($newPair, $count);
        }

        $this->addChar($this->formel[$pair], $count);
    }

    private function caSplit(string $pair): bool
    {
        return array_key_exists($pair, $this->formel);
    }

    private function reducePair(string $pair, int $count): void
    {
        !array_key_exists($pair, $this->pairs) || $this->pairs[$pair] -= $count;
    }

    private function gc(): void
    {
        $this->pairs = array_filter($this->pairs, static function ($counter) {
            return $counter > 0;
        });
    }
}