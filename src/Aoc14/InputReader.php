<?php
declare(strict_types=1);

namespace App\Aoc14;

final class InputReader
{
    private ?string $template;
    private array $formel = [];
    private string $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function init(): void
    {
        $this->parse(file_get_contents($this->filePath));
    }

    private function parse(string $fileContent): void
    {
        $this->reset();

        $file = explode(PHP_EOL, $fileContent);
        $this->template = array_shift($file);

        foreach ($file as $line) {
            $line = trim($line);
            if (empty($line)) {
                continue;
            }
            $line = str_replace(' ', '', $line);
            $formel = explode('->', $line);
            $this->formel[$formel[0]] = $formel[1];
        }
    }

    private function reset(): void
    {
        $this->template = null;
        $this->formel = [];
    }

    public function initTest(): void
    {
        $this->parse($this->getTestRawData());
    }

    public function getTestRawData(): string
    {
        return 'NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C';
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function getFormel(): array
    {
        return $this->formel;
    }
}