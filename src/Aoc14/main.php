<?php
declare(strict_types=1);

namespace App\Aoc14;

//ini_set('memory_limit', '16G');

require_once '../../vendor/autoload.php';

$main = new Main('./resources/input1.txt');
echo sprintf('Part 1 (test): %d', $main->runTest(10)) . PHP_EOL;
echo sprintf('Part 1: %d', $main->run(10)) . PHP_EOL;


echo sprintf('Part 1 (test): %d', $main->runTest(40)) . PHP_EOL;
echo sprintf('Part 1: %d', $main->run(40)) . PHP_EOL;
