<?php
declare(strict_types=1);

namespace App\Aoc14;

final class Main
{
    private InputReader $inputReader;

    public function __construct(string $filePath)
    {
        $this->inputReader = new InputReader($filePath);
    }

    public function runTest(int $until): int
    {
        $this->inputReader->initTest($until);
        return $this->_run($until);
    }

    public function run(int $until): int
    {
        $this->inputReader->init($until);
        return $this->_run($until);
    }

    private function _run(int $until): int
    {
        $template = $this->inputReader->getTemplate();
        $formel = $this->inputReader->getFormel();

        $polymer = Polymer::create($template, $formel);
        while($until > 0) {
            $polymer->step();
            --$until;
        }

        return $polymer->getQuantity();
    }
}