<?php
declare(strict_types=1);

namespace App\Aoc11;

final class Main
{
    private InputReader $inputReader;

    public function __construct(string $filePath)
    {
        $this->inputReader = new InputReader($filePath);
    }

    public function runTest1(int $steps): int
    {
        $this->inputReader->initTest();
        return $this->_run1($steps);
    }

    public function run1(int $steps): int
    {
        $this->inputReader->init();
        return $this->_run1($steps);
    }

    private function _run1(int $steps): int
    {
        $points = $this->inputReader->getPoints();

        $grid = Grid::create($points);
//        $grid->dumpPoints();
        while($steps > 0) {
            $grid->stepForward();
//            $grid->dumpPoints();
            --$steps;
        }

//        $grid->dumpPoints();

        return $grid->getFlushes();
    }


    public function runTest2(): int
    {
        $this->inputReader->initTest();
        return $this->_run2();
    }

    public function run2(): int
    {
        $this->inputReader->init();
        return $this->_run2();
    }

    private function _run2(): int
    {
        $points = $this->inputReader->getPoints();

        $grid = Grid::create($points);
        while(true) {
            $grid->stepForward();
            if ($grid->isFullFlash()) {
                return $grid->getStep();
            }
        }

        return -1; // not found
    }

}