<?php
declare(strict_types=1);


namespace App\Aoc11;

final class Grid
{
    private int $step = 0;
    /** @var array<int, int> */
    private array $grid;
    /** @var array<string, bool> */
    private array $alreadyFlushed = [];
    private array $flushes = [];

    private function __construct(array $grid)
    {
        $this->grid = $grid;
    }


    public static function create(array $grid): self
    {
        return new self($grid);
    }

    /**
     * @return int
     */
    public function getFlushes(): int
    {
        return array_sum($this->flushes);
    }

    public function isFullFlash(): bool
    {
        return $this->flushes[$this->step] === 100;
    }

    /**
     * @return int
     */
    public function getStep(): int
    {
        return $this->step;
    }


    public function stepForward(): self
    {
        $this->alreadyFlushed = [];
        $this->step++;
        $this->flushes[$this->step] = 0;

        foreach ($this->grid as $i => $row) {
            foreach ($row as $j => $column) {
                $this->increase($i, $j);
            }
        }

        return $this;
    }

    private function increase(int $i, int $j): self
    {
        if ($this->alreadyFlushed($i, $j)) {
            return $this;
        }

        $this->grid[$i][$j]++;
        if ($this->grid[$i][$j] > 9) {
            $this->flush($i, $j);
        }

        return $this;
    }

    private function flush(int $i, int $j): void
    {
        $this->alreadyFlushed[$i . 'x' . $j] = true;
        $this->grid[$i][$j] = 0;
        foreach ($this->findNeighbours($i, $j) as $neighbour) {
            $this->increase($neighbour[0], $neighbour[1]);
        }
        ++$this->flushes[$this->step];
    }

    private function findNeighbours(int $iCurrent, int $jCurrent): array
    {
        $top = isset($this->grid[$iCurrent - 1][$jCurrent]) ? [$iCurrent - 1, $jCurrent] : null;
        $down = isset($this->grid[$iCurrent + 1][$jCurrent]) ? [$iCurrent + 1, $jCurrent] : null;
        $left = isset($this->grid[$iCurrent][$jCurrent - 1]) ? [$iCurrent, $jCurrent - 1] : null;
        $right = isset($this->grid[$iCurrent][$jCurrent + 1]) ? [$iCurrent, $jCurrent + 1] : null;

        $topLef = isset($this->grid[$iCurrent - 1][$jCurrent - 1]) ? [$iCurrent - 1, $jCurrent - 1] : null;
        $topRight = isset($this->grid[$iCurrent - 1][$jCurrent + 1]) ? [$iCurrent - 1, $jCurrent + 1] : null;
        $downLeft = isset($this->grid[$iCurrent + 1][$jCurrent - 1]) ? [$iCurrent + 1, $jCurrent - 1] : null;
        $downRight = isset($this->grid[$iCurrent + 1][$jCurrent + 1]) ? [$iCurrent + 1, $jCurrent + 1] : null;

        return array_filter(
            [$left, $topLef, $top, $topRight, $right, $downRight, $down, $downLeft],
            static function ($value) {
                return $value !== null;
            }
        );
    }

    public function dumpPoints(): void
    {
        $lines = array_map(
            static function ($line) {
                return implode('', $line);
            },
            $this->grid
        );
        echo sprintf('Step: %d', $this->step) . PHP_EOL;
        foreach ($lines as $line) {
            echo $line . PHP_EOL;
        }
        echo sprintf('Flushes: %d', $this->flushes) . PHP_EOL;
        echo PHP_EOL;
        echo PHP_EOL;
    }

    private function alreadyFlushed(int $i, int $j): bool
    {
        return $this->alreadyFlushed[$i . 'x' . $j] ?? false;
    }

}