<?php
declare(strict_types=1);

namespace App\Aoc11;

final class InputReader
{
    private array $points;
    private string $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function init(): void
    {
        $this->parse(file_get_contents($this->filePath));
    }

    private function parse(string $fileContent): void
    {
        $this->reset();

        $file = explode(PHP_EOL, $fileContent);

        foreach ($file as $line) {
            $line = trim($line);
            $this->points[] = array_map(static function ($input) {
                return intval($input);
            }, str_split($line));
        }
    }

    private function reset(): void
    {
        $this->points = [];
    }

    public function initTest(): void
    {
        $this->parse($this->getTestRawData());
    }

    public function getTestRawData(): string
    {
        return '5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526';
    }

    public function getPoints(): array
    {
        return $this->points;
    }
}