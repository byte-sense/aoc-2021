<?php
declare(strict_types=1);

namespace App\Aoc8;

final class Main
{
    private InputReader $inputReader;

    public function __construct(string $filePath)
    {
        $this->inputReader = new InputReader($filePath);
    }

    public function runTest1(): int
    {
        $this->inputReader->initTest();
        return $this->_run1();
    }

    public function run1(): int
    {
        $this->inputReader->init();
        return $this->_run1();
    }

    private function _run1(): int
    {
        $inputSignals = $this->inputReader->getSignals();

        $easyDigits=0;
        foreach ($inputSignals as $inputSignal) {
            $signal = Signal::create($inputSignal['uniqueSignalPatterns'], $inputSignal['fourDigitOutputValue']);
            $easyDigits += $signal->getEasyDigits();
        }

        return $easyDigits;
    }

    public function runTest2(): int
    {
        $this->inputReader->initTest();
        return $this->_run2();
    }

    public function run2(): int
    {
        $this->inputReader->init();
        return $this->_run2();
    }

    private function _run2(): int
    {
        $inputSignals = $this->inputReader->getSignals();
        $result = 0;
        $table = [];
        foreach ($inputSignals as $inputSignal) {
            $signal = Signal::create($inputSignal['uniqueSignalPatterns'], $inputSignal['fourDigitOutputValue']);
            $result += $signal->decode();
        }

        return $result;
    }
}