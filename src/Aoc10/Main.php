<?php
declare(strict_types=1);

namespace App\Aoc10;

final class Main
{
    private InputReader $inputReader;

    public function __construct(string $filePath)
    {
        $this->inputReader = new InputReader($filePath);
    }

    public function runTest1(): int
    {
        $this->inputReader->initTest();
        return $this->_run1();
    }

    private function _run1(): int
    {
        $result = 0;
        $wrongClosing = [];
        $openingCharacters = ['<', '(', '[', '{'];
        $closingCharacters = ['>', ')', ']', '}'];
        $points = [
            '>' => 25137,
            ')' => 3,
            ']' => 57,
            '}' => 1197
        ];
        $mapClosingToOpening = array_combine($closingCharacters, $openingCharacters);

        $isClosingCharacter = static function ($value) use ($closingCharacters) {
            return in_array($value, $closingCharacters, true);
        };

        $charactersLines = $this->inputReader->getCharacters();
        foreach ($charactersLines as $characters) {
            for ($i = 0; $i < count($characters); $i++) {
                if ($i === 0) continue;
                $current = $characters[$i];
                $last = $characters[$i - 1] ?? null;
                if ($isClosingCharacter($current)) {
                    if ($last === $mapClosingToOpening[$current]) {
                        unset($characters[$i], $characters[$i - 1]);
                        $characters = array_values($characters);
                        $i = 0; // go back to first position
                    } else {
                        array_key_exists($current, $wrongClosing) || $wrongClosing[$current] = 0;
                        ++$wrongClosing[$current];
                        break;
                    }
                }
            }
        }

        foreach ($wrongClosing as $character => $count) {
            $result += $count * $points[$character];
        }

        return $result;
    }

    public function run1(): int
    {
        $this->inputReader->init();
        return $this->_run1();
    }

    public function runTest2(): int
    {
        $this->inputReader->initTest();
        return $this->_run2();
    }

    private function _run2(): int
    {
        $missingClosings = [];
        $openingCharacters = ['<', '(', '[', '{'];
        $closingCharacters = ['>', ')', ']', '}'];
        $points = [
            ')' => 1,
            ']' => 2,
            '}' => 3,
            '>' => 4
        ];

        $mapClosingToOpening = array_combine($closingCharacters, $openingCharacters);
        $mapOpeningToClosing = array_combine($openingCharacters, $closingCharacters);
        $isClosingCharacter = static function ($value) use ($closingCharacters) {
            return in_array($value, $closingCharacters, true);
        };


        $charactersLines = $this->inputReader->getCharacters();
        foreach ($charactersLines as $j => $characters) {
            for ($i = 0; $i < count($characters); $i++) {
                if ($i === 0) continue;
                $current = $characters[$i];
                $last = $characters[$i - 1] ?? null;
                if ($isClosingCharacter($current)) {
                    if ($last === $mapClosingToOpening[$current]) {
                        unset($characters[$i], $characters[$i - 1]);
                        $characters = array_values($characters);
                        $i = 0; // go back to first position
                    } else {
                        unset($charactersLines[$j]);
                        break;
                    }
                }
            }

            if (isset($charactersLines[$j])) {
                $missingClosings[] = array_reverse(
                    array_map(static function ($character) use ($mapOpeningToClosing) {
                        return $mapOpeningToClosing[$character];
                    }, $characters)
                );
            }
        }

        $scores = [];
        foreach ($missingClosings as $missingClosing) {
            $score = 0;
            foreach ($missingClosing as $index => $closingCharacter) {
                $score = ($score * 5) + $points[$closingCharacter];
            }
            $scores[] = $score;

        }

        sort($scores);
        return $scores[count($scores) / 2];
    }

    public function run2(): int
    {
        $this->inputReader->init();
        return $this->_run2();
    }
}