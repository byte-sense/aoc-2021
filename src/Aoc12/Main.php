<?php
declare(strict_types=1);

namespace App\Aoc12;

final class Main
{
    private InputReader $inputReader;

    public function __construct(string $filePath)
    {
        $this->inputReader = new InputReader($filePath);
    }

    public function runTest(int $times): int
    {
        $this->inputReader->initTest($times);
        return $this->_run($times);
    }

    public function run(int $times): int
    {
        $this->inputReader->init($times);
        return $this->_run($times);
    }

    private function _run(int $times): int
    {
        $connections = $this->inputReader->getConnections();
        $map = Map::create($connections);

        $map->calculateAllPossiblePaths($times);
//        $map->dumpPaths();
        return $map->countAllPossiblePaths();
    }
}