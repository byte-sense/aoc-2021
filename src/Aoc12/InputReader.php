<?php
declare(strict_types=1);

namespace App\Aoc12;

final class InputReader
{
    private array $connections;
    private string $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function init(): void
    {
        $this->parse(file_get_contents($this->filePath));
    }

    private function parse(string $fileContent): void
    {
        $this->reset();

        $file = explode(PHP_EOL, $fileContent);

        foreach ($file as $line) {
            $line = trim($line);
            $this->connections[] = str_getcsv($line, '-');
        }
    }

    private function reset(): void
    {
        $this->connections = [];
    }

    public function initTest(): void
    {
        $this->parse($this->getTestRawData());
    }

    public function getTestRawData(): string
    {
//        return 'start-A
//start-b
//A-c
//A-b
//b-d
//A-end
//b-end';

//        return 'dc-end
//HN-start
//start-kj
//dc-start
//dc-HN
//LN-dc
//HN-end
//kj-sa
//kj-HN
//kj-dc';
        return 'fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW';
    }

    public function getConnections(): array
    {
        return $this->connections;
    }
}