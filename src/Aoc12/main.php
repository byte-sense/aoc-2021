<?php
declare(strict_types=1);

namespace App\Aoc12;

require_once '../../vendor/autoload.php';

$main = new Main('./resources/input1.txt');
echo sprintf('Part 1 (test): %d', $main->runTest(1)) . PHP_EOL;
echo sprintf('Part 1: %d', $main->run(1)) . PHP_EOL;

$main = new Main('./resources/input2.txt');
echo sprintf('Part 1 (test): %d', $main->runTest(2)) . PHP_EOL;
echo sprintf('Part 1: %d', $main->run(2)) . PHP_EOL;
