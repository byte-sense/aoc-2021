<?php
declare(strict_types=1);


namespace App\Aoc12;

final class Map
{
    const START = 'start';
    const END = 'end';

    private array $connections = [];
    private array $paths = [];

    private function __construct(array $connections)
    {
        $this->parse($connections);
    }

    public static function create(array $connections): self
    {
        return new self($connections);
    }

    public function calculateAllPossiblePaths(int $times): void
    {
        $this->paths = $this->findPathsFrom(self::START, [], $times);
    }

    public function findPathsFrom(string $cave, array $visited, int $times): array
    {
        if ($this->isEnd($cave)) {
            return [[$cave]];
        }

        $paths = [];
        $this->visit($cave, $visited);
        foreach ($this->connections[$cave] as $destinationCave) {
            if ($this->isStart($destinationCave)) {
                continue;
            }

            if (!$this->canVisit($destinationCave, $visited, $times)) {
                continue;
            }

            foreach ($this->findPathsFrom($destinationCave, $visited, $times) as $item) {
                $paths[] = array_merge([$cave], $item);
            }
        }

        return $paths;
    }

    public function countAllPossiblePaths(): int
    {
        return count($this->paths);
    }

    public function dumpPaths(): void
    {
        foreach ($this->paths as $path) {
            echo implode(',', $path) . PHP_EOL;
        }
    }

    private function parse(array $connections): void
    {
        foreach ($connections as $connection) {
            array_key_exists($connection[0], $this->connections) || $this->connections[$connection[0]] = [];
            $this->connections[$connection[0]][] = $connection[1];

            // bi-directional connections
            if ($connection[0] !== self::START && $connection[1] !== self::END) {
                array_key_exists($connection[1], $this->connections) || $this->connections[$connection[1]] = [];
                $this->connections[$connection[1]][] = $connection[0];
            }
        }
    }

    private function visit(string $cave, array &$visited): void
    {
        array_key_exists($cave, $visited) || $visited[$cave] = 0;
        $visited[$cave]++;

        if ($this->isSmall($cave)) {
            array_key_exists('allSmallCounter', $visited) || $visited['allSmallCounter'] = [];
            array_key_exists($cave, $visited['allSmallCounter']) || $visited['allSmallCounter'][$cave] = 0;
            $visited['allSmallCounter'][$cave] = $visited[$cave];
        }
    }

    private function isSmall(string $destinationCave): bool
    {
        if ($this->isStart($destinationCave)) {
            return false;
        }
        if ($this->isEnd($destinationCave)) {
            return false;
        }

        return preg_match('/^[a-z]+$/', $destinationCave) === 1;
    }

    private function isEnd(string $cave): bool
    {
        return $cave === self::END;
    }

    private function isStart(string $cave): bool
    {
        return $cave === self::START;
    }

    private function canVisit(string $destinationCave, array $visited = [], int $times = 1): bool
    {
        if (!$this->isSmall($destinationCave)) {
            return true;
        }

        if(!isset($visited[$destinationCave])) {
            return true;
        }

        if($visited[$destinationCave] < 1) {
            return true;
        }

        if($times > 1) {
            $max = max($visited['allSmallCounter']);
            if ($max < $times) {
                return  true;
            }
        }

        return false;
    }
}