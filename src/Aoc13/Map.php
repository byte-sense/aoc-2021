<?php
declare(strict_types=1);


namespace App\Aoc13;

final class Map
{
    private array $map = [];
    private array $folds = [];
    private int $foldStep = 0;

    private function __construct(array $dots, array $folds)
    {
        $this->folds = $folds;

        $maxX = max(array_column($dots, 1));
        $maxY = max(array_column($dots, 0));
        $max = max($maxX, $maxY);
        $this->map = [];
        for ($x = 0; $x <= $max; $x++) {
            $this->map[$x] = [];
            for ($y = 0; $y <= $max; $y++) {
                $this->map[$x][$y] = false;
            }
        }

        foreach ($dots as $dot) {
            $this->map[$dot[1]][$dot[0]] = true;
        }
    }


    public static function create(array $dots, array $folds): self
    {
        return new self($dots, $folds);
    }

    public function countDots(): int
    {
        $counter = 0;
        foreach ($this->map as $line) {
            $counter += count(array_filter($line));
        }

        return $counter;
    }

    public function fold(): void
    {
        $fold = $this->folds[$this->foldStep];
        if ($fold[0] === 'x') {
            $this->foldVertically((int)$fold[1]);
        } elseif ($fold[0] === 'y') {
            $this->foldHorizontally((int)$fold[1]);
        }

        ++$this->foldStep;
    }

    private function foldVertically(int $line): void
    {
        $this->spin();
        $this->foldHorizontally($line);
        $this->spin();
    }

    private function foldHorizontally(int $line): void
    {
        $slice = array_slice($this->map, $line + 1);
        $this->map = array_slice($this->map, 0, $line);
        $slice = array_slice($slice, 0, count($this->map));

        $count = count($slice);
        while($count > 0) {
            $newX = ($line - $count);
            $moveLine = array_pop($slice);
            foreach ($moveLine as $y => $dot) {
                $this->map[$newX][$y] = ($this->map[$newX][$y] || $dot);
            }
            $count = count($slice);
        }
    }

    private function spin(): void
    {
        $map = [];
        foreach ($this->map as $i => $row) {
            foreach ($row as $j => $cell) {
                $map[$j][$i] = $cell;
            }
        }

        $this->map = $map;
    }

    public function dump(): void
    {
        foreach ($this->map as $row) {
            foreach ($row as $cell) {
                echo ($cell)  ? '#' : '.';
            }

            echo PHP_EOL;
        }
    }
}