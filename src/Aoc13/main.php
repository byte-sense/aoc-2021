<?php
declare(strict_types=1);

namespace App\Aoc13;

ini_set('memory_limit', '1G');

require_once '../../vendor/autoload.php';

$main = new Main('./resources/input1.txt');
echo sprintf('Part 1 (test): %d', $main->runTest()) . PHP_EOL;
echo sprintf('Part 1: %d', $main->run()) . PHP_EOL;
