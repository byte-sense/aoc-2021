<?php
declare(strict_types=1);

namespace App\Aoc13;

final class Main
{
    private InputReader $inputReader;

    public function __construct(string $filePath)
    {
        $this->inputReader = new InputReader($filePath);
    }

    public function runTest(): int
    {
        $this->inputReader->initTest();
        return $this->_run();
    }

    public function run(): int
    {
        $this->inputReader->init();
        return $this->_run();
    }

    private function _run(): int
    {
        $dots = $this->inputReader->getDots();
        $folds = $this->inputReader->getFolds();

        $until= count($folds);
        $map = Map::create($dots, $folds);

        while($until > 0) {
            $map->fold();
            --$until;
        }

        $map->dump();

        return $map->countDots();
    }
}