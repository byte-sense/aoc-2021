<?php
declare(strict_types=1);

namespace App\Aoc13;

final class InputReader
{
    private array $dots;
    private array $folds;
    private string $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    public function init(): void
    {
        $this->parse(file_get_contents($this->filePath));
    }

    private function parse(string $fileContent): void
    {
        $this->reset();

        $file = explode(PHP_EOL, $fileContent);

        foreach ($file as $line) {
            $line = trim($line);
            if (empty($line)) {
                continue;
            }
            if (substr($line, 0, strlen('fold along ')) === 'fold along ') {
                $line = str_replace('fold along ', '', $line);
                $this->folds[] = str_getcsv($line, '=');
            } else {
                $this->dots[] = array_map(static function($value) {
                    return intval($value);
                }, str_getcsv($line, ','));
            }
        }
    }

    private function reset(): void
    {
        $this->dots = [];
        $this->folds = [];
    }

    public function initTest(): void
    {
        $this->parse($this->getTestRawData());
    }

    public function getTestRawData(): string
    {
        return '6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5';
    }

    public function getDots(): array
    {
        return $this->dots;
    }

    public function getFolds(): array
    {
        return $this->folds;
    }
}